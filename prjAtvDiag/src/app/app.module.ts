import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { CadImovelComponent } from './components/cad-imovel/cad-imovel.component';
import { ImoveisUrbanosComponent } from './components/imoveis-urbanos/imoveis-urbanos.component';
import { ImoveisRuraisComponent } from './components/imoveis-rurais/imoveis-rurais.component';
import { TopoComponent } from './components/home/topo/topo.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CadImovelComponent,
    ImoveisUrbanosComponent,
    ImoveisRuraisComponent,
    TopoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
