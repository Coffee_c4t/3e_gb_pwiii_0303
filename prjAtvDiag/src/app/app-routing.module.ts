import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CadImovelComponent } from './components/cad-imovel/cad-imovel.component';
import { ImoveisRuraisComponent } from './components/imoveis-rurais/imoveis-rurais.component';
import { ImoveisUrbanosComponent } from './components/imoveis-urbanos/imoveis-urbanos.component';

const routes: Routes = [ 
  { path: '', component: HomeComponent },
  { path: 'addimovel', component: CadImovelComponent },
  { path: 'rurais', component: ImoveisRuraisComponent},
  { path: 'urbanos', component: ImoveisUrbanosComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
